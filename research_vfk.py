import pymel.core as pmc
import maya.cmds as mc


name = ""
numJoints=20.0
numControls=3.0
controlRadius = 4.0
          
jointRadius=0.25
jointPrefix = 'joint_'
jointGroupPrefix ='vfk_grp_'
controlPrefix = 'CTRL_vfk_'
controlGroupPrefix = 'OFF_CTRL_vfk_'
boneTranslateAxis = '.tx'
boneUpAxis = [1,0,0]

'''
if close_on_create_chk.checkState() == qc.Qt.Checked:
    close
'''


### Get top and end joints, check for parents/children
sels = pmc.ls(sl=1)
topJoint = sels[0]
endJoint = sels[1]

try:
    print topJoint, ' and ', endJoint, ' selected.'
    ### pmc.listRelatives(topJoint, children=True, type='joint')
except IndexError:
    print 'Error: Select a joint and an immediate child joint.'

    

endChild = pmc.listRelatives(endJoint, c=True)
if endChild:
    linkJointEnd = pmc.duplicate(endJoint, parentOnly=True, n=endJoint + '_LINK')
    pmc.setAttr(linkJointEnd[0] + '.radius', jointRadius * 2)
    pmc.parent(endChild, linkJointEnd)
    pmc.parent(linkJointEnd, w=True)
    
topParent = pmc.listRelatives(topJoint, p=True)
if topParent:
    linkJointTop = pmc.duplicate(topJoint, parentOnly=True,  n=topJoint + '_LINK')
    pmc.setAttr(linkJointTop[0] + '.radius', jointRadius * 2)
    pmc.parent(linkJointTop, topParent)
    pmc.parent(topJoint, linkJointTop)
        
### Check basic user-defined values

name = str(topJoint) + '_'


### Check advanced user-defined values
                      
boneUpAxis = [0,0,1]            
    
#### ACTUAL FUNCTION PART
nurbsWidth = pmc.getAttr(endJoint + boneTranslateAxis)

parentJoint=topJoint
boneTranslateAxis = '.tx'
add=numJoints-2
name="subJoint_"

selJoints = pmc.ls(selection=True)

boneLength = mc.getAttr(endJoint + boneTranslateAxis)
boneRadius = mc.getAttr(parentJoint + ".radius")

mc.select(cl=True)

for i in xrange(int(round(add))):
    newJoint = pmc.joint(name=name + str(i+1), radius = (boneRadius*2))
    mc.select(cl=True)
    pmc.parent(newJoint, parentJoint, relative=True)
    mc.setAttr(newJoint + boneTranslateAxis, (boneLength/(add + 1)))
    if i > 0:
        pmc.parent(newJoint, (name + str(i)), relative = True)
    if i == add - 1:
        pmc.parent(endJoint, newJoint)

surface = pmc.nurbsPlane(pivot=[0,0,0], axis= boneUpAxis, width=nurbsWidth, lengthRatio=0.1, 
                         u=(numJoints-1), ch=0, n= name + 'vfk_surface')

## Uncomment to use as polyPlane instead of nurbsSurface
## surface = pmc.polyPlane(w=20, h=1, sx=20, sy=1, ax=[0,1,0], cuv=2, ch=0, n= name + 'vfk_surface')

if boneTranslateAxis == '.ty':
    if boneUpAxis == [1,0,0]:
        pmc.setAttr(surface[0] + '.rx', -90)
    if boneUpAxis == [0,0,1]:
        pmc.setAttr(surface[0] + '.rz', -90)
    pmc.makeIdentity(surface[0], apply=True, t=0, r=1, s=0)

if boneTranslateAxis == '.tz':
    if boneUpAxis == [0,1,0]:
        pmc.setAttr(surface[0] + '.ry', -90)
    pmc.makeIdentity(surface[0], apply=True, t=0, r=1, s=0)            

surface_off = pmc.group(surface, n= name + 'OFF_surface')

pmc.parent(surface_off, topJoint)
if boneTranslateAxis == '.tx':
    pmc.xform(surface_off, translation = [nurbsWidth/2, 0, 0], rotation=[0,0,0])
if boneTranslateAxis == '.ty':
    pmc.xform(surface_off, translation = [0, nurbsWidth/2, 0], rotation=[0,0,0])        
if boneTranslateAxis == '.tz':
    pmc.xform(surface_off, translation = [0, 0, nurbsWidth/2], rotation=[0,0,0])        
pmc.parent(surface_off, w=True)

surface_mtx= pmc.xform(surface, q=True, ws=True, m=True)

joints = pmc.listRelatives(topJoint, children=1, allDescendents=1)
joints.append(topJoint)
joints.reverse()

for j in xrange(len(joints)):
    pmc.select(joints[j])
    pmc.rename(joints[j], jointPrefix + str(j+1))
    print joints[j]  
    print pmc.listRelatives(joints[j], parent=True)   
    pmc.setAttr(joints[j] + '.radius', jointRadius)
    pmc.addAttr(joints[j], ln='position', min=0, max=1, dv=0, keyable=True)
    pmc.setAttr(joints[j] + '.position', j/(numJoints-1))
    pmc.select(cl=1)
    jmtx = pmc.xform(joints[j], q=True, m=True, ws=True)
    
    if j == 0:
        off_vfk = pmc.group(em=True, n=  name + 'OFF_vfk')
        pmc.xform(off_vfk, ws=True, m=jmtx)
        root = pmc.listRelatives(joints[0], parent=True)      
        for c in xrange(int(numControls)):

            jparent = pmc.listRelatives(joints[j], parent=True)

            vfk_grp = pmc.group(em=True, n= name + jointGroupPrefix + 'j' + str(j+1) + '_c' + str(c+1))
            pmc.xform(vfk_grp, ws=True, m=jmtx)
            pmc.parent(joints[j], vfk_grp)
            if jparent:
                pmc.parent(vfk_grp.name(), jparent[0])
            if c == 0:
                pmc.parent(vfk_grp, off_vfk)
        if root:
            pmc.parent(off_vfk, root[0])
    else:
        for c in xrange(int(numControls)):
            jparent = pmc.listRelatives(joints[j], parent=True)
            vfk_grp = pmc.group(em=True, n= name + jointGroupPrefix + 'j' + str(j+1) + '_c' + str(c+1))
            pmc.xform(vfk_grp, ws=True, m=jmtx)
            pmc.parent(joints[j], vfk_grp)
            pmc.parent(vfk_grp, jparent[0])

ctrlSpacing = (nurbsWidth/(numControls+1))

for i in xrange(int(numControls)):
    if boneTranslateAxis == '.tx':
        ctrl_normal = [1,0,0]
    if boneTranslateAxis == '.ty':
        ctrl_normal = [0,1,0]
    if boneTranslateAxis == '.tz':
        ctrl_normal = [0,0,1]
    ctrl = pmc.circle(normal=ctrl_normal, sw=360, r=controlRadius, ch=0, n= name + controlPrefix + str(i+1))
    ctrl_off = pmc.group(ctrl, n= name + controlGroupPrefix + str(i+1))
    pmc.xform(ctrl_off, ws=True, m=surface_mtx)
                
    pmc.parent(ctrl_off, surface[0])
    pmc.setAttr(ctrl[0] + boneTranslateAxis, ((nurbsWidth/-2) + (ctrlSpacing*(i+1))))
    pmc.parent(ctrl_off, w=True)
    
    constrained_obj=ctrl
    geo=surface[0]
    deleteCPOMS=1
    cpos = pmc.createNode('closestPointOnSurface', n='cpos_flcl_' + geo)

    mc.connectAttr(pmc.listRelatives(geo, shapes=True, children=True)[0] + '.local', cpos + '.inputSurface')
    obj_mtx = pmc.xform(constrained_obj, q=True, m=True)
    pmc.setAttr(cpos + '.inPosition', [obj_mtx[12], obj_mtx[13], obj_mtx[14]])

    flclShape = pmc.createNode('follicle', n='flclShape' + geo)
    flcl = pmc.listRelatives(flclShape, type='transform', parent=True)
    pmc.rename(flcl, 'flcl_' + geo + '_1')

    mc.connectAttr(flclShape + '.outRotate', flcl[0] + '.rotate')
    mc.connectAttr(flclShape + '.outTranslate', flcl[0] + '.translate')
    mc.connectAttr(geo + '.worldMatrix', flclShape + '.inputWorldMatrix')
    mc.connectAttr(geo + '.local', flclShape + '.inputSurface')
    mc.setAttr(flclShape + '.simulationMethod', 0)

    u = mc.getAttr(cpos + '.result.parameterU')
    v = mc.getAttr(cpos + '.result.parameterV')
    pmc.setAttr(flclShape + '.parameterU', u)
    pmc.setAttr(flclShape + '.parameterV', v)

    pmc.parent(constrained_obj, flcl[0])
    if deleteCPOMS == 1:
        pmc.delete(cpos)

    
    ctrl_mtx = pmc.xform(ctrl, q=True, m=True, ws=True)
    pmc.xform(ctrl_off, ws=True, m=ctrl_mtx)
    pmc.parent(ctrl_off, flcl[0])
    pmc.parent(ctrl, ctrl_off)
    
    min_falloff = 1/numJoints
    
    pmc.addAttr(ctrl[0], ln='position', min=0, max=10, dv=0, keyable=True)
    pmc.addAttr(ctrl[0], ln='falloff', min=min_falloff, max=1, dv=0.5, keyable=True)
    pmc.addAttr(ctrl[0], ln='numberOfJointsAffected', min=0, max=numJoints, dv=0, keyable=True)
    
    multD = pmc.createNode('multiplyDivide', n= name + 'multD_jAff_vfk_' + str(i+1))
    setR = mc.createNode('setRange', n= name + 'setR_jAff_vfk_' + str(i+1))
        
    pmc.connectAttr(ctrl[0] + '.falloff', multD + '.input1X')
    pmc.setAttr(multD + '.input2X', 2)
    pmc.setAttr(multD + '.operation', 1)
    
    pmc.connectAttr(multD + '.outputX', setR + '.valueX')
    pmc.setAttr(setR + '.oldMinX', 0)
    pmc.setAttr(setR + '.oldMaxX', 1)
    pmc.setAttr(setR + '.minX', 0)
    pmc.setAttr(setR + '.maxX', numJoints)
    pmc.connectAttr(setR + '.outValueX', ctrl[0] + '.numberOfJointsAffected')

       
    paramU = pmc.getAttr(flcl[0] + '.parameterU')
    
    div_ten = pmc.createNode('multiplyDivide', n="DIV_" + name + controlPrefix + str(i+1))
    pmc.setAttr(div_ten.input2X, 10)
    pmc.setAttr(div_ten.operation, 2)

    pmc.connectAttr(ctrl[0] + '.position', div_ten.input1X)

    pmc.connectAttr(div_ten.outputX, flcl[0] + '.parameterU')

    pmc.setAttr(ctrl[0] + '.position', paramU * 10.0)
    
    fPos_plus = pmc.createNode('plusMinusAverage', n= name + 'fPosPlus_vfk_' + str(i+1))
    pmc.connectAttr(div_ten.outputX, fPos_plus + '.input1D[0]', f=True)
    pmc.connectAttr(ctrl[0] + '.falloff', fPos_plus + '.input1D[1]', f=True)
    pmc.setAttr(fPos_plus + '.operation', 1)
    
    fPos_minus = pmc.createNode('plusMinusAverage', n=  name + 'fPosMinus_vfk_' + str(i+1))
    pmc.connectAttr(div_ten.outputX, fPos_minus + '.input1D[0]', f=True)
    pmc.connectAttr(ctrl[0] + '.falloff', fPos_minus + '.input1D[1]', f=True)
    pmc.setAttr(fPos_minus + '.operation', 2)
    
    for f in (fPos_plus, fPos_minus):
        for j in xrange(len(joints)):
            upperM = pmc.createNode('plusMinusAverage', n= (name + f + '_upperM_j' + str(j+1) + '_c' + str(i+1)))
            lowerM = pmc.createNode('plusMinusAverage', n= (name + f + '_lowerM_j' + str(j+1) + '_c' + str(i+1)))
            
            pmc.setAttr(upperM + '.operation', 2)
            pmc.setAttr(lowerM + '.operation', 2)
            
            pmc.connectAttr(joints[j] + '.position', upperM + '.input1D[0]')
            pmc.connectAttr(f + '.output1D', upperM + '.input1D[1]')
            
            pmc.connectAttr(div_ten.outputX, lowerM + '.input1D[0]')
            pmc.connectAttr(f + '.output1D', lowerM + '.input1D[1]')
            
            divA = pmc.createNode('multiplyDivide', n= f + '_divA_j' + str(j+1) + '_c' + str(i+1))
            pmc.setAttr(divA + '.operation', 2)
            pmc.connectAttr(upperM + '.output1D', divA + '.input1X')
            pmc.connectAttr(lowerM + '.output1D', divA + '.input2X')
            
            multA = pmc.createNode('multiplyDivide', n= f + '_multA_j' + str(j+1) + '_c' + str(i+1))
            pmc.setAttr(multA + '.operation', 1)
            pmc.connectAttr(divA + '.outputX', multA + '.input1X')
            pmc.setAttr(multA + '.input2X', 2)

            divB = pmc.createNode('multiplyDivide', n= f + '_divB_j' + str(j+1) + '_c' + str(i+1))
            pmc.setAttr(divB + '.operation', 2)
            pmc.connectAttr(multA + '.outputX', divB + '.input1X')
            pmc.connectAttr(ctrl[0] + '.numberOfJointsAffected', divB + '.input2X')
            
            
    for j in xrange(len(joints)):
        cond = pmc.createNode('condition', n= name + 'cond_j' + str(j+1) + '_c' + str(i+1))
        pmc.setAttr(cond + '.operation', 3)
        pmc.connectAttr(div_ten.outputX, cond + '.firstTerm') # then use minus
        pmc.connectAttr(joints[j] + '.position', cond + '.secondTerm') # then use plus

        pmc.connectAttr(fPos_minus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfTrueR')
        pmc.connectAttr(fPos_minus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfTrueG')
        pmc.connectAttr(fPos_minus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfTrueB')

        pmc.connectAttr(fPos_plus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfFalseR')
        pmc.connectAttr(fPos_plus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfFalseG')
        pmc.connectAttr(fPos_plus + '_divB_j' + str(j+1) + '_c' + str(i+1) + '.outputX', cond + '.colorIfFalseB')
        
        cond_neg = pmc.createNode('condition', n= name + 'cond_neg_j' + str(j+1) + '_c' + str(i+1))
        pmc.connectAttr(cond + '.outColorR', cond_neg + '.firstTerm')
        pmc.setAttr(cond_neg + '.secondTerm', 0)
        pmc.setAttr(cond_neg + '.operation', 2)
        pmc.connectAttr(cond + '.outColor', cond_neg + '.colorIfTrue')
        pmc.setAttr(cond_neg + '.colorIfFalse', [0,0,0])
        
        multiFinalRot = pmc.createNode('multiplyDivide', n=  name + 'multiFinalRot_j' + str(j+1) + '_c' + str(i+1))
        pmc.setAttr(multiFinalRot + '.operation', 1)
        pmc.connectAttr(cond_neg + '.outColor', multiFinalRot + '.input1')
        pmc.connectAttr(ctrl[0] + '.rotate', multiFinalRot + '.input2')
        
        pmc.connectAttr(multiFinalRot + '.output', name + jointGroupPrefix + 'j' + str(j+1) + '_c' + str(i+1) + '.rotate')

        '''
        multiFinalScl = pmc.createNode('multiplyDivide', n=  name + 'multiFinalScl_j' + str(j+1) + '_c' + str(i+1))
        pmc.setAttr(multiFinalScl + '.operation', 1)
        pmc.connectAttr(cond + '.outColor', multiFinalScl + '.input1')
        pmc.connectAttr(ctrl[0] + '.scale', multiFinalScl + '.input2')
        
        pmc.connectAttr(multiFinalScl + '.output', name + jointGroupPrefix + 'j' + str(j+1) + '_c' + str(i+1) + '.scale')
        '''
    pmc.undoInfo(openChunk=True)
    controls =ctrl

    if controls == []:
        controls = pmc.ls(sl=True)

    for control in controls:
        control_roo = pmc.xform(control, q=True, roo=True)
        control_mtx = pmc.xform(control, q=True, m=True, ws=True)
        control_parent = pmc.listRelatives(control, p=True)
        pmc.select(cl=True)
        
        locdbl_parent = pmc.spaceLocator(n='locDBL_parent_' + control)
        locdbl_offset = pmc.spaceLocator(n='locDBL_offset_' + control)

        pmc.xform(locdbl_parent, ws=True, m=control_mtx)
        pmc.xform(locdbl_offset, ws=True, m=control_mtx)
            
        pmc.parent(locdbl_offset, locdbl_parent)
        pmc.parent(locdbl_parent, control_parent[0])
        pmc.parent(control, locdbl_offset)
        
        if control_roo == 'xyz':
            pmc.xform(locdbl_offset, roo='zyx')
        if control_roo == 'yzx':
            pmc.xform(locdbl_offset, roo='xzy')        
        if control_roo == 'zxy':
            pmc.xform(locdbl_offset, roo='yxz')        
        if control_roo == 'xzy':
            pmc.xform(locdbl_offset, roo='yzx')
        if control_roo == 'yxz':
            pmc.xform(locdbl_offset, roo='zxy')            
        if control_roo == 'zyx':
            pmc.xform(locdbl_offset, roo='xyz')
                        
        md_trns = pmc.createNode('multiplyDivide', n='mdTRNS_locDBL_' + control)
        md_rot = pmc.createNode('multiplyDivide', n='mdROT_locDBL_' + control)
        md_scl = pmc.createNode('multiplyDivide', n='mdSCL_locDBL_' + control)
        
        pmc.setAttr(md_trns + '.input1', [-1,-1,-1])
        pmc.setAttr(md_rot.input1, [-1,-1,-1])
        pmc.setAttr(md_scl.input1, [ 1, 1, 1])
        pmc.setAttr(md_scl.operation, 2)

        
        pmc.connectAttr(control + '.translate', md_trns + '.input2')
        pmc.connectAttr(control + '.rotate', md_rot + '.input2')
        pmc.connectAttr(control + '.scale', md_scl + '.input2')
        
        pmc.connectAttr(md_trns + '.output', locdbl_offset + '.translate')
        pmc.connectAttr(md_rot + '.output', locdbl_offset + '.rotate')
        pmc.connectAttr(md_scl + '.output', locdbl_offset + '.scale')
        
        pmc.setAttr(locdbl_parent + 'Shape.visibility', 0)
        pmc.setAttr(locdbl_offset + 'Shape.visibility', 0)
        
    pmc.undoInfo(closeChunk=True)

if endChild:
    pmc.parent(linkJointEnd, endJoint)

pmc.undoInfo(closeChunk=True)
pmc.undoInfo(openChunk=True)
    
pmc.skinCluster(joints, name + 'vfk_surface', mi=1)

pmc.undoInfo(closeChunk=True)


    ####################
    # INTERNAL FUNCTIONS
    ####################
