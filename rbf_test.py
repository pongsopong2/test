import pymel.core as pm

sel = pm.ls(sl=True)
target = sel[-1]
sel.remove(target)

rbf = pm.createNode('weightDriver')
pm.setAttr('{}.type'.format(rbf), 1)

target_node = pm.PyNode(target)
target.translate.tx >> rbf.input[0]
target.translate.ty >> rbf.input[1]
target.translate.tz >> rbf.input[2]

i=0
for s in sel:
	pyNode = pm.PyNode(s)
	pyNode.translate.tx >> rbf.poses[i].poseInput[0]
	pyNode.translate.ty >> rbf.poses[i].poseInput[1]
	pyNode.translate.tz >> rbf.poses[i].poseInput[2]
	
	pyNode.rotate.rx >> rbf.poses[i].poseValue[0]
	pyNode.rotate.ry >> rbf.poses[i].poseValue[1]
	pyNode.rotate.rz >> rbf.poses[i].poseValue[2]
	
	pyNode.scale.sx >> rbf.poses[i].poseValue[3]
	pyNode.scale.sy >> rbf.poses[i].poseValue[4]
	pyNode.scale.sz >> rbf.poses[i].poseValue[5]
	i+=1
	
rbf.output[0] >> target.rotate.rx 
rbf.output[1] >> target.rotate.ry
rbf.output[2] >> target.rotate.rz 

rbf.output[3] >> target.scale.sx 
rbf.output[4] >> target.scale.sy
rbf.output[5] >> target.scale.sz






