import pymel.core as pm

sel = pm.ls(sl=True)
target = sel[-1]
sel.remove(target)

dcpm_target = pm.createNode('decomposeMatrix')
pm.addAttr(target, k=True, ln='ampa', dv=0.00)
target.worldMatrix[0] >> dcpm_target.inputMatrix

for driver in sel:
	pma_pos = pm.createNode('plusMinusAverage')
	pma_pos.operation.set(2)
	driver.translate >> pma_pos.input3D[0]
	dcpm_target.outputTranslate >> pma_pos.input3D[1]
	
	vp_dot = pm.createNode('vectorProduct')
	vp_dot.operation.set(1)
	pma_pos.output3D >> vp_dot.input1
	pma_pos.output3D >> vp_dot.input2
	
	mul_sqr = pm.createNode('multiplyDivide')
	mul_sqr.operation.set(3)
	mul_sqr.input2X.set(0.5)
	mul_sqr.input2Y.set(0.5)
	mul_sqr.input2Z.set(0.5)
	vp_dot.output >> mul_sqr.input1
	
	mul_dv = pm.createNode('multiplyDivide')
	mul_dv.operation.set(2)
	target.amp >> mul_dv.input2X
	target.amp >> mul_dv.input2Y
	target.amp >> mul_dv.input2Z
	mul_sqr.output >> mul_dv.input1
	
	pma_sub = pm.createNode('plusMinusAverage')
	pma_sub.operation.set(2)
	pma_sub.input3D[0].input3Dx.set(1)
	pma_sub.input3D[0].input3Dy.set(1)
	pma_sub.input3D[0].input3Dz.set(1)
	mul_dv.output >> pma_sub.input3D[1]
	
	cdt = pm.createNode('condition')
	cdt.operation.set(2)
	cdt.colorIfFalseR.set(0)
	cdt.colorIfFalseG.set(0)
	cdt.colorIfFalseB.set(0)
	pma_sub.output3D.output3Dx >> cdt.firstTerm
	pma_sub.output3D >> cdt.colorIfTrue
	
	
	
	
	 