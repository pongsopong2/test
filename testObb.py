import pymel.core as pm
import maya.OpenMaya as OpenMaya
from numpy import linalg as LA

sels = pm.ls(sl=True, fl=True)
pointSize = float(len(sels))
mPoints = OpenMaya.MPointArray()
for sel in sels:
  point = cmds.xform('pSphere1.vtx[280]',q=True,ws=True,t=True)
  mPoints.append(point[0],point[1],point[2])
mVecPoints = OpenMaya.MVectorArray()
[mVecPoints.append(OpenMaya.MVector(mPoints[x]))for x in xrange(mPoints.length())]

mu = OpenMaya.MVector(0.0, 0.0, 0.0)
dir(mu)
# Calculate the average position of points.
for p in xrange(int(pointSize)):
  print mVecPoints[p]/ pointSize
  mu += mVecPoints[p] / pointSize

cxx, cxy, cxz, cyy, cyz, czz = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
for p in xrange(int(pointSize)):
  p = mVecPoints[p]
  cxx += p.x * p.x - mu.x * mu.x
  cxy += p.x * p.y - mu.x * mu.y
  cxz += p.x * p.z - mu.x * mu.z
  cyy += p.y * p.y - mu.y * mu.y
  cyz += p.y * p.z - mu.y * mu.z
  czz += p.z * p.z - mu.z * mu.z
pm.spaceLocator(p = [cxx,cxy,cxz])
pm.spaceLocator(p = [cxy, cyy, cyz])
pm.spaceLocator(p = [cxz, cyz, czz])
pm.spaceLocator(p = [  50.00003008,  225.00010799,  225.00016851])
pm.spaceLocator(p = [  1.27240945e-10,  -7.83892088e-01,  -6.20897089e-01])
pm.spaceLocator(p = [  6.58411287e-01,   4.67323373e-01,  -5.90002918e-01])
pm.spaceLocator(p = [  7.52658340e-01,  -4.08805651e-01,   5.16123398e-01])
pm.spaceLocator(p = [ 1.27240945e-10, 4.67323373e-01,   5.16123398e-01])
pm.spaceLocator(p = [  -7.83892088e-01, -5.90002918e-01,   7.52658340e-01])
# Covariance Matrix
C = [[cxx, cxy, cxz],
  [cxy, cyy, cyz],
  [cxz, cyz, czz]]

eigenValues, eigVec = LA.eigh(C)

r = OpenMaya.MVector(eigVec[0][0], eigVec[1][0], eigVec[2][0])
r.normalize()

u = OpenMaya.MVector(eigVec[0][1], eigVec[1][1], eigVec[2][1])
u.normalize()

f = OpenMaya.MVector(eigVec[0][2], eigVec[1][2], eigVec[2][2])
f.normalize()

minim = OpenMaya.MVector(1e10, 1e10, 1e10)
maxim = OpenMaya.MVector(-1e10, -1e10, -1e10)

for i in xrange(mVecPoints.length()):
    pnt = mVecPoints[i]

    p_prime = OpenMaya.MVector(
        r * pnt, u * pnt, f * pnt)

    minim = OpenMaya.MVector(
        min(minim.x, p_prime.x),
        min(minim.y, p_prime.y),
        min(minim.z, p_prime.z))
    maxim = OpenMaya.MVector(
        max(maxim.x, p_prime.x),
        max(maxim.y, p_prime.y),
        max(maxim.z, p_prime.z))

centerPoint = (maxim + minim) * .5
m_ext = (maxim - minim) * .5

R = OpenMaya.MVector(r.x, u.x, f.x)
U = OpenMaya.MVector(r.y, u.y, f.y)
F = OpenMaya.MVector(r.z, u.z, f.z)

m_pos = OpenMaya.MVector(
    R * centerPoint, U * centerPoint, F * centerPoint)
    
pm.spaceLocator(p = [obb_extents.x, obb_extents.y, obb_extents.z])

eigenVectors, center, obb_extents = [r, u, f], m_pos, m_ext

boundPoints = [(center - eigenVectors[0] *
  obb_extents.x + eigenVectors[1] *
  obb_extents.y + eigenVectors[2] *
  obb_extents.z),
  (center - eigenVectors[0] *
  obb_extents.x + eigenVectors[1] *
  obb_extents.y - eigenVectors[2] *
  obb_extents.z),
  (center + eigenVectors[0] *
  obb_extents.x + eigenVectors[1] *
  obb_extents.y + eigenVectors[2] *
  obb_extents.z),
  (center + eigenVectors[0] *
  obb_extents.x + eigenVectors[1] *
  obb_extents.y - eigenVectors[2] *
  obb_extents.z),
  (center + eigenVectors[0] *
  obb_extents.x - eigenVectors[1] *
  obb_extents.y + eigenVectors[2] *
  obb_extents.z),
  (center + eigenVectors[0] *
  obb_extents.x - eigenVectors[1] *
  obb_extents.y - eigenVectors[2] *
  obb_extents.z),
  (center - eigenVectors[0] *
  obb_extents.x - eigenVectors[1] *
  obb_extents.y + eigenVectors[2] *
  obb_extents.z),
  (center - eigenVectors[0] *
  obb_extents.x - eigenVectors[1] *
  obb_extents.y - eigenVectors[2] *
  obb_extents.z)]

_width = (boundPoints[1] - boundPoints[0]).length()
_height = (boundPoints[2] - boundPoints[0]).length()
_depth = (boundPoints[6] - boundPoints[0]).length()

m = [(eigenVectors[1].x * obb_extents.y * 2),
     (eigenVectors[1].y * obb_extents.y * 2),
     (eigenVectors[1].z * obb_extents.y * 2),
     0.0,
     (eigenVectors[2].x * obb_extents.z * 2),
     (eigenVectors[2].y * obb_extents.z * 2),
     (eigenVectors[2].z * obb_extents.z * 2),
     0.0,
     (eigenVectors[0].x * obb_extents.x * 2),
     (eigenVectors[0].y * obb_extents.x * 2),
     (eigenVectors[0].z * obb_extents.x * 2),
     0.0,
     center.x,
     center.y,
     center.z,
     1.0]

# Get the scale.
mMat = OpenMaya.MMatrix()
OpenMaya.MScriptUtil.createMatrixFromList(m, mMat)

if mMat.det4x4() < 0:
    m[8] *= -1
    m[9] *= -1
    m[10] *= -1
print m
lattice = cmds.lattice(dv=(2, 2, 2),
                       objectCentered=True,
                       name="aaa")
cmds.xform(lattice[1], matrix=m)
cmds.xform(lattice[2], matrix=m)
