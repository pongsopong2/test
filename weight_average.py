import maya.cmds as mc
import pymel.core as pmc
import maya.OpenMaya as om

def weights_flow(jnts, geo, chain)
	vtc = mc.ls('{}.vtx[*]'.format(geo), fl=True)
	skin = mm.eval('findRelatedSkinCluster {}'.format(geo))
	skin_node = pmc.PyNode(skin)
	infs = skin_node.getInfluence()
	max_weight = 1.0 / chain
	max_distance = wa.get_distance(jnts[0], jnts[chain])
	ref_back_jnt_vec = wa.get_invert_position(jnts[0], jnts[chain])
	ref_front_jnt_vec = wa.get_invert_position(jnts[-1], jnts[(chain*-1)-1])
	vtx_front = []
	vtx_back = []
	for vtx in vtc:
	    weights = [ix*0 for ix in range(len(infs))]
	    jnt_index = [ix for ix in range(len(infs))]
	    for ix, jnt in enumerate(jnts):
	        project_vtx_vec = wa.get_position_by_dot_product(jnt, ref_front_jnt_vec, vtx)
	        current_distance = wa.get_distance(jnt, project_vtx_vec)
	        if not current_distance.length() >= max_distance.length():
	            weight =  (1.0 - (current_distance.length()/max_distance.length())) * max_weight
	            index = wa.get_index_influence(jnt, infs)
	            weights[index] = round(weight, 8)

	    diraction_back = wa.get_position_front_back(jnts[(chain*-1)-1], ref_front_jnt_vec, ref_back_jnt_vec, vtx)
	    diraction_front = wa.get_position_front_back(jnts[chain], ref_front_jnt_vec, ref_back_jnt_vec, vtx)
	    print diraction_back
	    if diraction_front == 'back':
	        index = wa.get_index_influence(jnts[0], infs)
	        weights[index] += 1 - sum(weights)
	    elif diraction_back == 'front':
	        index = wa.get_index_influence(jnts[-1], infs)
	        weights[index] += 1 - sum(weights)

	    skin_node.setWeights(vtx, jnt_index, weights, normalize=True)

def get_vector(obj):
	if not type(obj) == om.MVector:
		pos = mc.xform(obj, t=True, q=True, ws=True)
		vec = om.MVector(pos[0], pos[1], pos[2])
		return vec
	else:
		return obj


def get_distance(obj_a, obj_b):
	a_vec = get_vector(obj_a)
	b_vec = get_vector(obj_b)

	distance = b_vec - a_vec
	return distance


def get_position_by_dot_product(base, obj_a, obj_b):
	base_vec = get_vector(base)
	distance_a = get_distance(base, obj_a)
	distance_b = get_distance(base, obj_b)

	dot_pos = (distance_a * ((distance_a * distance_b) / (distance_a.length() ** 2))) + base_vec
	return dot_pos


def get_invert_position(obj_a, obj_b):
	a_vec = get_vector(obj_a)
	b_vec = get_vector(obj_b)

	invert_pos = ((b_vec - a_vec) * -1) + a_vec
	return invert_pos


def get_direction(vec_a, vec_b):
    if round(vec_a.x, 12) >= round(vec_b.x, 12):
        x = True
    else:
        x = False
    if round(vec_a.y, 12) >= round(vec_b.y, 12):
        y = True
    else:
        y = False
    if round(vec_a.z, 12) >= round(vec_b.z, 12):
        z = True
    else:
        z = False

    return [x, y, z]


def get_position_front_back(current_jnt, front_jnt_vec, back_jnt_vec, vtx):
	current_jnt_vec = get_vector(current_jnt)
	vtx_vec = get_position_by_dot_product(current_jnt, front_jnt_vec, vtx)
	fornt_direction = get_direction(current_jnt_vec, front_jnt_vec)
	back_direction = get_direction(current_jnt_vec, back_jnt_vec)
	vtx_direction = get_direction(current_jnt_vec, vtx_vec)

	if vtx_direction == fornt_direction:
		return 'front'
	else:
		return 'back'


def get_order_front_jnt(jnts, ix, chain):
    check_num = 0
    length = 0
    num = ix
    jnt_order = list()
    length_order = list()
    for i in range(chain):
        if jnts[num] == jnts[-1]:
            num = ix - 1
            check_num = 1
            length += get_distance(jnts[ix], jnts[num]).length()
            uturn = length
        elif check_num == 1:
            num -= 1
            length = get_distance(jnts[ix], jnts[num]).length() + uturn
        else:
            num += 1
            length = get_distance(jnts[ix], jnts[num]).length()
        jnt_order.append(jnts[num])
        length_order.append(length)
    return jnt_order, length_order


def get_order_back_jnt(jnts, ix, chain):
    check_num = 0
    length = 0
    num = ix
    jnt_order = list()
    length_order = list()
    for i in range(chain):
        if jnts[num] == jnts[0]:
            num = ix + 1
            check_num = 1
            length += get_distance(jnts[ix], jnts[num]).length()
            uturn = length
        elif check_num == 1:
            num += 1
            length = get_distance(jnts[ix], jnts[num]).length() + uturn
        else:
            num -= 1 
            length = get_distance(jnts[ix], jnts[num]).length()
        jnt_order.append(jnts[num])
        length_order.append(length)
    return jnt_order, length_order


def get_index_influence(jnt, infs):
	for ix, inf in enumerate(infs):
		if inf == jnt:
			return ix


